<?php


namespace Drupal\banner_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use StringTranslationTrait;

/**
 * Provides a 'Banner' block.
 *
 * @Block(
 *   id = "banner_block",
 *   admin_label = @Translation("Banner block"),
 *   category = @Translation("Blocks")
 * )
 */
class BannerBlock extends BlockBase
{
  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state)
  {
    $form = parent::blockForm($form, $form_state);
    // Retrieve existing configuration for this block.
    $config = $this->getConfiguration();
    $form['banner_block'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Banner block'),
    ];
    $image = NULL;
    if (isset($config['image'])) {
      $image = Media::load($config['image']);
    }
    $form['banner_block']['image'] = [
      '#type' => 'media_library',
      '#allowed_bundles' => ['banner_block'],
      '#title' => t('Add image'),
      '#description' => t('Upload or select the image.'),
      '#default_value' => !empty($image) ? $image->get('mid')->value : NULL,
      '#required' => TRUE,
    ];
    $form['banner_block']['link'] = [
      '#type' => 'url',
      '#title' => $this->t('Banner Link'),
      '#default_value' => isset($config['link']) ? $config['link'] : '',
    ];
    $form['banner_block']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title banner'),
      '#default_value' => isset($config['title']) ? $config['title'] : '',
    ];
    $form['banner_block']['title_display'] = array(
      '#type' => 'select',
      '#title' => $this->t('Display title in'),
      '#default_value' => isset($config['title_display']) ? $config['title_display'] : NULL,
      '#options' => [
        'hidden' => $this->t('Hidden'),
        'before' => $this->t('Before the banner'),
        'about' => $this->t('About the banner'),
        'after' => $this->t('After the banner')
      ],
    );
    $form['banner_block']['text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Text'),
      '#default_value' => isset($config['text']) ? $config['text'] : '',
    ];
    $form['banner_block']['text_display'] = array(
      '#type' => 'select',
      '#title' => $this->t('Display text in'),
      '#default_value' => isset($config['text_display']) ? $config['text_display'] : NULL,
      '#options' => [
        'hidden' => $this->t('Hidden'),
        'before' => $this->t('Before the banner'),
        'about' => $this->t('About the banner'),
        'after' => $this->t('After the banner')
      ],
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    // Save our custom settings when the form is submitted.
    $this->setConfigurationValue('image', !empty($values['banner_block']['image']) ? $values['banner_block']['image'] : NULL);
    $this->setConfigurationValue('link', !empty($values['banner_block']['link']) ? $values['banner_block']['link'] : '');
    $this->setConfigurationValue('title', !empty($values['banner_block']['title']) ? $values['banner_block']['title'] : '');
    $this->setConfigurationValue('title_display',!empty($values['banner_block']['title_display']) ? $values['banner_block']['title_display'] : '');
    $this->setConfigurationValue('text', !empty($values['banner_block']['text']) ? $values['banner_block']['text'] : '');
    $this->setConfigurationValue('text_display', !empty($values['banner_block']['text_display']) ? $values['banner_block']['text_display'] : '');
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $config = $this->getConfiguration();
    if (isset($config['image'])) {
      $image = Media::load($config['image']);
      $file_image_id = $image->get('field_media_image')->target_id;
      $file_image = File::load($file_image_id);
      $file_image_uri = $file_image->getFileUri();
    }
    $link = isset($config['link']) ? $config['link'] : '';
    $title = isset($config['title']) ? $config['title'] : '';
    $title_display = isset($config['title_display']) ? $config['title_display'] : '';
    $text = isset($config['text']) ? $config['text'] : '';
    $text_display = isset($config['text_display']) ? $config['text_display'] : '';
    return [
      '#title_banner' => $title,
      '#title_display' => $title_display,
      '#text_banner' => $text,
      '#text_display' => $text_display,
      '#image' => isset($config['image']) ? $image->get('field_media_image')->getValue() : NULL,
      '#image_url' => isset($file_image_uri) ? $file_image_uri : '',
      '#link' => $link,
      '#theme' => 'banner_block',
    ];
  }

}
